package com.example.bankappdigicore.model;

import com.example.bankappdigicore.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class Account {
    private long id;
   private String accountName;
   private String accountNumber;
   private String password;
   private Double accountBalance;
   private TransactionType transactionType;
   private Date transactionDate;
   private String narration;
}
