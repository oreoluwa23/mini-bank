package com.example.bankappdigicore.controller;


import com.example.bankappdigicore.dto.AccountInfo;
import com.example.bankappdigicore.dto.AccountStatementDTO;
import com.example.bankappdigicore.payload.ApiResponse;
import com.example.bankappdigicore.service.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {

@Autowired
    private AccountServiceImpl accountService;

    @RequestMapping(value = "/account_info/{accountNumber}", method = RequestMethod.GET)
    public ResponseEntity<AccountInfo> getAccountInfo (@PathVariable (value = "accountNumber") String accountNumber){
        ApiResponse response = new ApiResponse(Boolean.TRUE, "success", HttpStatus.OK);
        HttpHeaders httpHeaders = new HttpHeaders();
        String message = "success";
        httpHeaders.add("message", message);
        AccountInfo accountInfo = accountService.getAccountDetails(accountNumber);
        return new ResponseEntity<>(accountInfo, httpHeaders, HttpStatus.OK);
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.POST)
    public ResponseEntity<ApiResponse> deposit(@RequestBody String accountNumber,
                                    @RequestParam double amount) throws Exception {
        ApiResponse response = accountService.accountDeposit(accountNumber, amount);
        HttpStatus status = response.isSuccess() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(response, status);
    }

    @RequestMapping(value = "/withdrawal", method = RequestMethod.POST)
    public ResponseEntity<ApiResponse> withdrawal(@RequestParam String accountNumber,
                                                  @RequestParam String password,
                                                  @RequestParam double withdrawnAmount){
        ApiResponse response = accountService.accountWithdrawal(accountNumber,withdrawnAmount,password);
        HttpStatus status = response.isSuccess()?HttpStatus.OK : HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(response, status);

    }

    @RequestMapping(value = "/account_statement/{accountNumber}")
    public ResponseEntity<AccountStatementDTO> transactionHistory(@PathVariable (value = "accountNumber") String accountNumber){
        return new ResponseEntity<>(accountService.getAccountTransaction(accountNumber), HttpStatus.ACCEPTED);

    }




}
