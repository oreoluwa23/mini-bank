package com.example.bankappdigicore.configuration;

import com.example.bankappdigicore.model.Account;
import com.example.bankappdigicore.service.AccountService;
import com.example.bankappdigicore.service.AccountServiceImpl;
import lombok.SneakyThrows;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.security.auth.login.AccountNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException {
        AccountService accountService = new AccountServiceImpl();
        List<Account> accountList = accountService.getAccounts();
        Account account1 = new Account();
        for (Account account : accountList) {
            if (account.getAccountName().equalsIgnoreCase(accountName)) {
                account1 = account;
            }
        }
        if (account1 == null) {
            throw new AccountNotFoundException("Account not found");
        } else {
            return new User(account1.getAccountName(),account1.getPassword(),new ArrayList<>());
        }

//        UserBuilder userBuilder = null;
//        AccountService service = new AccountServiceImpl();
//        for (Account account : service.getAccounts()){
//            if (account.getAccountName().equalsIgnoreCase(accountName)){
//                userBuilder =  org.springframework.security.core.userdetails.User.withUsername(accountName);
////                userBuilder.password(new BCryptPasswordEncoder().encode(account.getPassword()));
//            }else {
//                throw new AccountNotFoundException("account not found");
//            }
//        }

//        Account user = user.orElseThrow(() ->
//                new UsernameNotFoundException("No user found with email : " + userEmail));
//
//        if(user.getIsEnabled()){
//            return new UserDetails() {
//                @Override
//                public Collection<? extends GrantedAuthority> getAuthorities() {
//                    return null;
//                }
//
//                @Override
//                public String getPassword() {
//                    return null;
//                }
//
//                @Override
//                public String getUsername() {
//                    return null;
//                }
//
//                @Override
//                public boolean isAccountNonExpired() {
//                    return false;
//                }
//
//                @Override
//                public boolean isAccountNonLocked() {
//                    return false;
//                }
//
//                @Override
//                public boolean isCredentialsNonExpired() {
//                    return false;
//                }
//
//                @Override
//                public boolean isEnabled() {
//                    return false;
//                }
//            }UserDetails(user);
//        }
//        throw new AccountNotEnabledException("Account is disabled");
//    }
//        return userBuilder.build();
    }
}
