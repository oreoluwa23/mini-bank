package com.example.bankappdigicore.dto;

import com.example.bankappdigicore.enums.TransactionType;
import com.example.bankappdigicore.model.Account;
import lombok.Data;

import java.util.Date;
@Data
public class AccountStatementDTO {
    long id;
    Account account;
    Date transactionDate;
    TransactionType transactionType;
    String narration;
    Double amount;
    Double accountBalance;
    String password;
}
