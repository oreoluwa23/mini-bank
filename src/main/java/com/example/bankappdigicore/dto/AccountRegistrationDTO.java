package com.example.bankappdigicore.dto;

import lombok.Data;

@Data
public class AccountRegistrationDTO {
    long accountId;
    String accountName;
    String accountPassword;
    Double initialDeposit = 500.0;

}
