package com.example.bankappdigicore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankAppDigiCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAppDigiCoreApplication.class, args);
    }

}
