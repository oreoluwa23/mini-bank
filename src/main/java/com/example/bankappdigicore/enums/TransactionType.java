package com.example.bankappdigicore.enums;

public enum TransactionType {
    WITHDRAWAL,
    DEPOSIT
}
