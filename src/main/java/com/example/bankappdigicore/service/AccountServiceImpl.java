package com.example.bankappdigicore.service;

import com.example.bankappdigicore.dto.AccountInfo;
import com.example.bankappdigicore.dto.AccountRegistrationDTO;
import com.example.bankappdigicore.dto.AccountStatementDTO;
import com.example.bankappdigicore.enums.TransactionType;
import com.example.bankappdigicore.model.Account;
import com.example.bankappdigicore.payload.ApiResponse;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
@Data
public class AccountServiceImpl implements AccountService{

    List<Account> accountList= new ArrayList<>();
    List<AccountStatementDTO> transactionList= new ArrayList<>();

        public static  String randomNumber(){
            SecureRandom random = new SecureRandom();
            String accountNum = "";
            for (int counter = 0; counter <=5; counter++)
                accountNum += random.nextInt(25);
            System.out.println(accountNum.length());
            return accountNum;
        }

    @Override
    public List<Account> getAccounts() {
        return accountList;
    }

    @Override
    public Account getAccount(long courseId) {
        return null;
    }

    @Override
    public ApiResponse createAccount(AccountRegistrationDTO accountRegistrationDTO) {
        Account account = new Account();
        ApiResponse response = new ApiResponse();


        account.setAccountName(accountRegistrationDTO.getAccountName());
        account.setAccountNumber(randomNumber());
        account.setPassword(accountRegistrationDTO.getAccountPassword());
        account.setAccountBalance(accountRegistrationDTO.getInitialDeposit());
        account.setId(accountRegistrationDTO.getAccountId());
        System.out.println(account.getAccountName());
        if(accountList.size()==0){
            accountList.add(account);
            response.setSuccess(true);
            response.setMessage("The account successfully created");
            response.setStatus(HttpStatus.OK);

        }else{
            int count =0;
            for(Account account1:accountList){
                if(account1.getAccountName().equalsIgnoreCase(account.getAccountName())){

                    count++;
                }
            }
            if(count>0){
                response.setStatus(HttpStatus.CONFLICT);
                response.setSuccess(false);
                response.setMessage("The account is already present");
            }
            else{
                response.setSuccess(true);
                response.setMessage("The account successfully created");
                response.setStatus(HttpStatus.OK);
                accountList.add(account);

            }
        }

//        for (Account account1 :accountList){
//            if (account1.getAccountName().equalsIgnoreCase(account.getAccountName())){
//                System.out.println(account1);
//                System.out.println(account1.getAccountName());
////                response = new ApiResponse(Boolean.TRUE, "The account is already present", HttpStatus.CONFLICT);
//                response.setSuccess(Boolean.FALSE);
//                response.setMessage("The account is already present");
//                response.setStatus(HttpStatus.CONFLICT);
//            }else {
//                accountList.add(account);
//                System.out.println("here it is");
////                response = new ApiResponse(Boolean.TRUE, "Account successfully created", HttpStatus.OK);

//            }
//        }
        return response;
    }


    @Override
    public ApiResponse accountDeposit(String accountNumber, double amount) {
        ApiResponse response = null;
        for (Account account : accountList) {
            if (account.getAccountNumber().equals(accountNumber)) {
                if (amount > 1000000 || amount < 1) {
                    response = new ApiResponse(Boolean.FALSE, "Limited amount reached");
                } else {
                    AccountStatementDTO accountStatementDTO = new AccountStatementDTO();
                    double previousBalance = account.getAccountBalance();
                    double newAccountBalance = previousBalance + amount;
                    account.setAccountBalance(newAccountBalance);
                    account.setTransactionDate(new Date());
                    account.setTransactionType(TransactionType.DEPOSIT);
                    accountStatementDTO.setAccountBalance(newAccountBalance);
                    accountStatementDTO.setAmount(amount);
                    accountStatementDTO.setId(account.getId());
                    accountStatementDTO.setTransactionDate(new Date());
                    accountStatementDTO.setTransactionType(TransactionType.DEPOSIT);
                    accountStatementDTO.setAccount(account);
                    transactionList.add(accountStatementDTO);
                    response = new ApiResponse(Boolean.TRUE, "deposit successful");
                }

            }else {
                response = new ApiResponse(Boolean.FALSE, "account not available");
            }
        }
        return response;
    }

    @Override
    public ApiResponse accountWithdrawal(String accountNumber, double amount, String password) {
            ApiResponse response = null;
            AccountStatementDTO accountStatementDTO = new AccountStatementDTO();

            for (Account account : accountList){
                if (account.getAccountNumber().equals(accountNumber)){
                    double previousBalance = account.getAccountBalance();
                    double newAccountBalance = previousBalance - amount;
                    if (newAccountBalance < 500.0){
                        response = new ApiResponse(Boolean.FALSE, "your balance is less than 500.00, deposit to withdraw", HttpStatus.BAD_REQUEST);
                    }else {
                        account.setAccountBalance(newAccountBalance);
                        account.setTransactionType(TransactionType.WITHDRAWAL);
                        account.setTransactionDate(new Date());
                        accountStatementDTO.setAccountBalance(newAccountBalance);
                        accountStatementDTO.setAmount(amount);
                        accountStatementDTO.setId(account.getId());
                        accountStatementDTO.setPassword(password);
                        accountStatementDTO.setTransactionDate(new Date());
                        accountStatementDTO.setTransactionType(TransactionType.WITHDRAWAL);
                        accountStatementDTO.setAccount(account);
                        transactionList.add(accountStatementDTO);
                        response = new ApiResponse(Boolean.TRUE, "withdrawal successful", HttpStatus.OK);
                    }
                }else {
                    response = new ApiResponse(Boolean.FALSE, "account does not exist", HttpStatus.UNAUTHORIZED);
                }
            }
        return response;
    }

    @Override
    public AccountStatementDTO getAccountTransaction(String accountNumber) {
            AccountStatementDTO statementDTO = new AccountStatementDTO();
          for (AccountStatementDTO account : transactionList){
              if (account.getAccount().getAccountNumber().equals(accountNumber)){
                  statementDTO = account;
              }
          }
        return statementDTO;
    }

    @Override
    public AccountInfo getAccountDetails(String accountNumber) {
        AccountInfo accountInfo = new AccountInfo();
            for (Account account : accountList){
                if (account.getAccountNumber().equalsIgnoreCase(accountNumber)){
                    accountInfo.setAccountName(account.getAccountName());
                    accountInfo.setAccountNumber(account.getAccountNumber());
                    accountInfo.setAccountBalance(account.getAccountBalance());
                }
            }
        return accountInfo;
    }
}
