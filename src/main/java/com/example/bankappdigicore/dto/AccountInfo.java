package com.example.bankappdigicore.dto;

import com.example.bankappdigicore.model.Account;
import lombok.Data;

@Data
public class AccountInfo {
    private String accountName;
    private String accountNumber;
    private double accountBalance;
}
