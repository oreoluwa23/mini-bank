package com.example.bankappdigicore.controller;

import com.example.bankappdigicore.configuration.JwtTokenUtil;
import com.example.bankappdigicore.configuration.UserDetailsServiceImpl;
import com.example.bankappdigicore.dto.AccountRegistrationDTO;
import com.example.bankappdigicore.dto.JwtRequest;
import com.example.bankappdigicore.dto.JwtResponse;
import com.example.bankappdigicore.payload.ApiResponse;
import com.example.bankappdigicore.service.AccountService;
import com.example.bankappdigicore.service.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    AccountService service = new AccountServiceImpl();
@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest jwtRequest) throws Exception {

        try {
            System.out.println(jwtRequest.getAccountName());
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken
                    (jwtRequest.getAccountName(), jwtRequest.getPassword()));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getAccountName());

        final String jwtToken = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(jwtToken));
    }
@RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<ApiResponse> registerAccount(@RequestBody AccountRegistrationDTO account) throws Exception {
       ApiResponse response = service.createAccount(account);
//    System.out.println(response.isSuccess());
        HttpStatus status = response.isSuccess() ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST;
    System.out.println(status);
        return new ResponseEntity<>(response, status);
    }
}
