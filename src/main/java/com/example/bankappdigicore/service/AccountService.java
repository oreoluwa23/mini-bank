package com.example.bankappdigicore.service;

import com.example.bankappdigicore.dto.AccountInfo;
import com.example.bankappdigicore.dto.AccountRegistrationDTO;
import com.example.bankappdigicore.dto.AccountStatementDTO;
import com.example.bankappdigicore.model.Account;
import com.example.bankappdigicore.payload.ApiResponse;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface AccountService {
    List<Account> getAccounts();
    Account getAccount(long courseId);
    ApiResponse createAccount(AccountRegistrationDTO account);
    ApiResponse accountDeposit(String accountNumber, double amount);
    ApiResponse accountWithdrawal(String accountNumber, double amount, String narration);
    AccountStatementDTO getAccountTransaction(String accountNumber);
    AccountInfo getAccountDetails(String accountNumber);
}
